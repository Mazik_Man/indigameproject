﻿#pragma strict
var waveText:Text;
var waveNumText:Text;
var coinsNum:Text;
var enemy : GameObject[];                
var spawnTime : float = 4f;            
var spawnPoints : Transform[];
var time:float;
var time2:float;       
var theplayerAtt:playerAtt;
var player:GameObject;
var numOfenemie:int;
var enemyArray:int[];
var check:int=0;
var checkWave:int=0;
var waveTry:boolean=true;
var enemyTry:boolean=true;
var timeCheck:boolean=false;
var compl:boolean;
static var eneNum:int;
var waveArray:int[];
var gameOver:String;
var sound:AudioClip;

function Start ()
{
	compl=false;
	timeCheck=false;
	enemyTry=true;
	check=0;
	eneNum = enemyArray[check];
	checkWave=0;
	waveText.text="Wave " + waveArray[checkWave];
	yield WaitForSeconds(5);
	waveText.text="";
  	InvokeRepeating ("Spawn", spawnTime, spawnTime);
}
function Update()
{
	waveNumText.text = "" + waveArray[checkWave];
	coinsNum.text = "" + money.money;
	time-=Time.deltaTime;
	time2-=Time.deltaTime;
	if(player!=null)
	{
    	theplayerAtt =player.GetComponent(playerAtt);
    }
    if(waveTry&&timeCheck==false)
    {
	    if(eneNum<=0)
	    {
	    	CancelInvoke("Spawn");
	    	time=8;
	    	checkWave++;
	    	waveTry=false;
	    }
    }
    if(numOfenemie>=enemyArray[check])
    {
    	CancelInvoke("Spawn");
    }
    if(waveTry==false&&timeCheck==false)
    {
    	waveText.text="Wave " + waveArray[checkWave];
    	if(time<=0)
    	{
    		eneNum = enemyArray[check];
    		waveText.text="";
	    	InvokeRepeating ("Spawn", spawnTime, spawnTime);
	    	check++;
	    	numOfenemie=0;
	    	waveTry=true;
	    	if(waveArray[checkWave]==waveArray[waveArray.Length-1])
	    	{
	    		timeCheck=true;
	    		compl=true;
	    	}
    	}
    }
    if(waveArray[checkWave]==waveArray[waveArray.Length-1]&&compl==true)
    {
    	if(eneNum<=0)
    	{
    		waveText.text="Level Complete";
    		GetComponent.<AudioSource>().PlayOneShot(sound);
    		time2=10;
    		CancelInvoke("Spawn");
    		enemyTry=false;
    		compl=false;
    	}
    }
    if(enemyTry==false&&time2<=0)
    {
    	Application.LoadLevel(gameOver);
    }
}

function Spawn ()
{
    if(theplayerAtt.curplayerHealth <= 0f)
    {
        return;
    }	
    var enemyRange : int = Random.Range (0, enemy.Length);
    Instantiate (enemy[enemyRange], spawnPoints[1].position, spawnPoints[1].rotation);
    numOfenemie++;
    
    if(numOfenemie<enemyArray[check])
    {
    	var enemyRange2 : int = Random.Range (0, enemy.Length);
    	Instantiate (enemy[enemyRange2], spawnPoints[0].position, spawnPoints[0].rotation);
    	numOfenemie++;
    }
}

