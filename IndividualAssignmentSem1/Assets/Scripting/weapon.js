﻿#pragma strict
var damageNumber:GameObject;
var hitPoint:Transform;
var deathParticle:GameObject;
static var playerDmg:int = 2;
static var playerStr:int = 1;
static var playerVit:int = 1;
static var playerWis:int = 1;
static var playerDex:int = 1;
static var playerSpd:float = 5;
static var playerHealth:float = 5;
static var playerIntDmg:int = 2;
static var playerInt:int = 1;
static var curDmg:int = 1;
static var curIntDmg:int = 4;
static var curIntHealth:int = 4;
static var spriteRenderer:SpriteRenderer;
var _color:Color;
var spriteSwords:Sprite[];
var sound:AudioClip;

function Start () 
{
	spriteRenderer = GameObject.FindWithTag("Weapon").gameObject.GetComponent(SpriteRenderer); 
}
function Awake()
{

}

function Update () 
{
	playerDmg=curDmg+playerStr;
	playerIntDmg=curIntDmg+playerInt;
	playerHealth=curIntHealth+playerInt;
	_color = new Color(0,0,0);
	WeaponUpdate();
}
function OnTriggerEnter2D(col: Collider2D) 
{
	Debug.Log ("Collision");
	if (col.gameObject.tag == "Enemy") 
	{
		col.gameObject.GetComponent(enemy).EnemyHit(playerDmg);
        Instantiate(deathParticle, col.transform.position, col.transform.rotation);
		var clone = Instantiate(damageNumber,col.transform.position, Quaternion.Euler(Vector3.zero));
		floatingNumbers.damageNumber = playerDmg;
		floatingNumbers._color = _color;
		floatingNumbers._text = "-";
		GetComponent.<AudioSource>().PlayOneShot(sound);
	}
	if (col.gameObject.tag == "Enemy2") 
	{
		col.gameObject.GetComponent(enemy2).EnemyHit(playerDmg);
        Instantiate(deathParticle, col.transform.position, col.transform.rotation);
		var clone2 = Instantiate(damageNumber,col.transform.position, Quaternion.Euler(Vector3.zero));
		floatingNumbers.damageNumber = playerDmg;
		floatingNumbers._color = _color;
		floatingNumbers._text = "-";
		GetComponent.<AudioSource>().PlayOneShot(sound);
	}
}
function WeaponUpdate()
{
	switch (BuyWeapon.weaponNum)
    {
    case 8:
		spriteRenderer.sprite = spriteSwords[7];
		weapon.curDmg=9;       
		break;
    case 7:
		spriteRenderer.sprite = spriteSwords[6];
		weapon.curDmg=8;       
		break;
    case 6:
		spriteRenderer.sprite = spriteSwords[5];
		weapon.curDmg=7;       
		break;
    case 5:
		spriteRenderer.sprite = spriteSwords[4];
		weapon.curDmg=6;       
		break;
    case 4:
        spriteRenderer.sprite = spriteSwords[3];
		weapon.curDmg=5;
        break;
    case 3:
        spriteRenderer.sprite = spriteSwords[2];
		weapon.curDmg=4;
        break;
    case 2:
        spriteRenderer.sprite = spriteSwords[1];
		weapon.curDmg=3;
        break;
    case 1:
        spriteRenderer.sprite = spriteSwords[0];
		weapon.curDmg=2;
        break;
    default:
        break;
    }
}