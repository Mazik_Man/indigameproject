﻿#pragma strict
var startGame:String;
var loading: GameObject;
var player:GameObject;
var controls:GameObject;

function Update()
{
	player = GameObject.Find("Player(Clone)");
}

function NewGame () 
{
	LoadingScreen.scene=3;
	loading.SetActive(true);
	ResetValues();	
	StatMenu.instanceNum=0;
	Player_controller.once_call=false;
	InstantiatePlayer.inst=true;
}

function LoadGame()
{
	LoadingScreen.scene=3;
	loading.SetActive(true);
	InstantiatePlayer.inst=true;
	Player_controller.once_call=false;
}
function LoadSavedGame()
{
	if(File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
	{
		LoadingScreen.scene=3;
		loading.SetActive(true);
		InstantiatePlayer.inst=true;
		Player_controller.once_call=false;
	}
}
function StartGame()
{
	LoadingScreen.scene=5;
	loading.SetActive(true);
}
function Menu()
{
	Destroy(player.gameObject);
	loading.SetActive(true);
	LoadingScreen.scene = 0;
}
function QuitGame () 
{
	Application.Quit();
}
function ResetValues()
{
	money.money = 0;
	weapon.playerDmg = 2;
 	weapon.playerStr = 1;
	weapon.playerVit = 1;
	weapon.playerWis = 1;
	weapon.playerDex = 1;
	weapon.playerSpd = 5;
	weapon.playerIntDmg = 2;
	weapon.playerInt = 1;
	weapon.curDmg = 1;
	weapon.curIntDmg = 4;
 	playerAtt.maxplayerHealth=100;
	playerAtt.maxplayerMana=100;
	playerAtt.currLevel=1;
	playerAtt.skillpoint=1;
	playerAtt.maxSkillpoint=1;
}
function Cont()
{
	controls.SetActive(true);
}
function NoCont()
{
	controls.SetActive(false);
}