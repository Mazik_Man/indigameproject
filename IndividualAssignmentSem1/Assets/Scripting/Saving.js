﻿#pragma strict
import System;
import System.Runtime.Serialization.Formatters.Binary;
import System.IO;

function Start () {

}

function Save() 
{
	var bf: BinaryFormatter = new BinaryFormatter();
	var file:FileStream = File.Create(Application.persistentDataPath + "/playerInfo.dat");
	var data:PlayerData = new PlayerData();
	
	data.money = money.money;
	data.playerDmg = weapon.playerDmg;
 	data.playerStr = weapon.playerStr;
	data.playerVit = weapon.playerVit;
	data.playerWis = weapon.playerWis;
	data.playerDex = weapon.playerDex;
	data.playerSpd = weapon.playerSpd;
	data.playerIntDmg = weapon.playerIntDmg;
	data.playerInt = weapon.playerInt;
	data.curDmg = weapon.curDmg;
	data.curIntDmg = weapon.curIntDmg;
	data.maxplayerHealth = playerAtt.maxplayerHealth;
	data.maxplayerMana = playerAtt.maxplayerMana;
	data.currLevel = playerAtt.currLevel;
	data.skillpoint = playerAtt.skillpoint;
	data.maxSkillpoint = playerAtt.maxSkillpoint;
	data.weaponNum = BuyWeapon.weaponNum;
	data.currExp = playerAtt.currExp;
	data.armor = playerAtt.armor;
	bf.Serialize(file, data);
	file.Close();
	print(Application.persistentDataPath);
	showSaveText.check=true;
}
function Load()
{
	if(File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
	{
		var bf: BinaryFormatter = new BinaryFormatter();
		var file:FileStream = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
		var data:PlayerData = bf.Deserialize(file);
		
		money.money = data.money;
		weapon.playerDmg = data.playerDmg;
 		weapon.playerStr = data.playerStr;
		weapon.playerVit = data.playerVit;
		weapon.playerWis = data.playerWis;
		weapon.playerDex = data.playerDex;
		weapon.playerSpd = data.playerSpd;
		weapon.playerIntDmg = data.playerIntDmg;
		weapon.playerInt = data.playerInt;
		weapon.curDmg = data.curDmg;
		weapon.curIntDmg = data.curIntDmg;
		playerAtt.maxplayerHealth = data.maxplayerHealth;
		playerAtt.maxplayerMana = data.maxplayerMana;
		playerAtt.currLevel = data.currLevel;
		playerAtt.skillpoint = data.skillpoint;
		playerAtt.maxSkillpoint = data.maxSkillpoint;
		BuyWeapon.weaponNum = data.weaponNum;
		playerAtt.currExp = data.currExp;
		playerAtt.armor = data.armor;
	}
}

class PlayerData
{
	var money: int;
	var playerDmg: int;
 	var playerStr: int;
	var playerVit: int;
	var playerWis: int;
	var playerDex: int;
	var playerSpd: float;
	var playerIntDmg: int;
	var playerInt: int;
	var curDmg: int;
	var curIntDmg: int;
	var maxplayerHealth: int;
	var maxplayerMana: int;
	var currLevel: int;
	var skillpoint: int;
	var maxSkillpoint: int;
	var weaponNum: int;
	var currExp:int;
	var armor:float;
}