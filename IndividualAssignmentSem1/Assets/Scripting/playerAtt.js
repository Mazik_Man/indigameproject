﻿#pragma strict
import UnityEngine.UI;

static var curplayerHealth:float;
static var maxplayerHealth:float=100;

static var curplayerMana:float;
static var maxplayerMana:float=100;

static var currLevel:int=1;
static var currExp:float;

static var skillpoint:int=1;
static var maxSkillpoint:int=1;

static var armor:float=0;
var gameOver:String;

static var lvl2:boolean=false;
static var lvlInf:boolean=false;

var loading: GameObject;
var script : Player_controller; 
var sound:AudioClip[];
var once:boolean=true;
private var anim :Animator;

function Start()
{
	anim = GetComponentInChildren(Animator);
}

function Awake () 
{
	curplayerHealth=maxplayerHealth;
	curplayerMana=maxplayerMana;
	once=true;
	script = GetComponent(Player_controller);
}

function Update()
{
	
	if(curplayerHealth>maxplayerHealth)
	{
		curplayerHealth=maxplayerHealth;
	}
	if(curplayerHealth<30&&once)
	{
		once=false;
		GetComponent.<AudioSource>().PlayOneShot(sound[0]);
	}
	if(currLevel>=5)
	{
		lvl2=true;
	}
	if(currLevel>=10)
	{
		lvlInf=true;
	}
	if(armor>0.99)
	{
		armor=0.99;
	}
	if(Application.loadedLevelName =="test")
	{
		script.enabled=false;
		anim.enabled=false;
		transform.localScale = new Vector3(1,1,1);
	}
	else
	{
		script.enabled=true;
		anim.enabled=true;
	}
}

function FixedUpdate () {
	
	if(curplayerHealth<=0)
	{	
		GetComponent.<AudioSource>().PlayOneShot(sound[0]);
		Application.LoadLevel(gameOver);
	}
	
}

function AddExp(expToAdd:int)
{
	currExp+=expToAdd;
}
function PlayerHit(enemydam:float)
{
	curplayerHealth -=enemydam;
}
function  OnLevelWasLoaded()
{
	curplayerHealth=maxplayerHealth;
}