﻿#pragma strict
import UnityEngine.UI;

var enemyHealthMax:float;
var enemyHealth:float;
var healthbar:Image;
var theplayerAtt:playerAtt;
var coins:GameObject;
var target:Transform;
var speed:float=3.0;
var walk:boolean;
var attack:boolean=true;
var xDis:float;
var expToGive:int;
var j:int;
var radius:float;
var randomAtt:float;
private var anim :Animator;

function Start () 
{
	anim = GetComponentInChildren(Animator);
	theplayerAtt = FindObjectOfType(playerAtt);
	radius=1;
}
function Awake()
{
	target = GameObject.FindGameObjectWithTag("Player").transform;
	enemyHealth=enemyHealthMax;
}
function Update () 
{
	healthbar.fillAmount = enemyHealth/enemyHealthMax;
	randomAtt-=Time.deltaTime;
	if(enemyHealth<=0)
	{
		Destroy(gameObject);
		theplayerAtt.AddExp(expToGive);
		for(j=0;j<=Random.Range(0,3);j++)
		{
			
			Instantiate(coins, Random.insideUnitSphere * radius+ transform.position, transform.rotation);
		}
		EnemyManager.eneNum-=1;
	}
	
	if(target!=null)
	{
		xDis=transform.position.x-target.position.x;
		if(xDis>1.5)
		{
			walk = true;
			anim.SetBool("walk",true);
			anim.SetBool("attack",false);
			transform.Translate(new Vector3(-speed*Time.deltaTime,0,0));
			transform.localScale=new Vector3(0.3,0.3,1);
		}
		
		if(xDis<1.5)
		{
			walk = false;
			anim.SetBool("walk",false);
			transform.Translate(new Vector3(0,0,0));
			transform.localScale=new Vector3(0.3,0.3,1);
			anim.SetBool("attack",true);
				
			
		}
		if(xDis<-1.5)
		{
			walk = true;
			anim.SetBool("walk",true);
			anim.SetBool("attack",false);
			transform.Translate(new Vector3(speed*Time.deltaTime,0,0));
			transform.localScale=new Vector3(-0.3,0.3,1);
		}
		if(xDis>-1.5&&xDis<0)
		{
			walk = false;
			anim.SetBool("walk",false);
			transform.Translate(new Vector3(0,0,0));
			transform.localScale=new Vector3(-0.3,0.3,1);
			anim.SetBool("attack",true);
		}
		else{
			walk=false;
		}
	}
}
function EnemyHit(playerdmg:int)
{
	enemyHealth -= playerdmg;
}