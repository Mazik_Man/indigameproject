﻿#pragma strict
var healthbar:Image;
var manabar:Image;
var expbar:Image;
var displayNumber:Text;
var sound:AudioClip;

var displayLvl:Text;
var toLevel:float[];
var LvlTime:float;
var spawnPoint : Transform;
var anim:Animator;
private var player:GameObject;
function Start () 
{
	player = GameObject.FindGameObjectWithTag("Player");
}
function Awake()
{
	anim = GetComponentInChildren(Animator);
}

function Update ()
 {
 	if(player!=null)
 	{
	 	transform.position = GameObject.FindWithTag("Player").transform.position;
	 	LvlTime -= Time.deltaTime;
	 	healthbar.fillAmount = playerAtt.curplayerHealth/playerAtt.maxplayerHealth;
	 	manabar.fillAmount = playerAtt.curplayerMana/playerAtt.maxplayerMana;
		expbar.fillAmount = playerAtt.currExp/toLevel[playerAtt.currLevel];
		displayNumber.text = "" + playerAtt.currLevel;
		if(playerAtt.currExp>= toLevel[playerAtt.currLevel])
		{
			GetComponent.<AudioSource>().PlayOneShot(sound);
			playerAtt.currLevel++;
			displayLvl.text = "Level UP";
			anim.SetTrigger("isActivated");
			LvlTime =2;
			playerAtt.currExp=0;
			playerAtt.skillpoint++;
			playerAtt.maxSkillpoint++;
		}
		if(LvlTime <=0)
		{
			displayLvl.text = "";
		}
	}
}
