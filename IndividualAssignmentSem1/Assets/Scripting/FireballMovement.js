﻿#pragma strict
var player:GameObject;
var damageNumber:GameObject;
var deathParticle:GameObject;
var movement:float;
var rigi:Rigidbody2D;
var sound:AudioClip[];
var _color:Color;

function Start () 
{
	GetComponent.<AudioSource>().PlayOneShot(sound[1]);
	rigi = GetComponent(Rigidbody2D);
	if(Player_controller.right==true)
	{
		rigi.velocity = new Vector2(movement,0);
	}
	else
	{
		rigi.velocity = new Vector2(-movement,0);
	}
	_color = new Color(0,0,0);
}

function OnTriggerEnter2D(other:Collider2D)
{
	if(other.gameObject.tag == "Enemy")
	{
		other.gameObject.GetComponent(enemy).EnemyHit(weapon.playerIntDmg);
		var clone = Instantiate(damageNumber,other.transform.position, Quaternion.Euler(Vector3.zero));
		floatingNumbers.damageNumber = weapon.playerIntDmg;
		Instantiate(deathParticle, other.transform.position, other.transform.rotation);
		floatingNumbers._color = _color;
		floatingNumbers._text = "-";
		Destroy(gameObject);
	}
	if(other.gameObject.tag == "Enemy2")
	{
		other.gameObject.GetComponent(enemy2).EnemyHit(weapon.playerIntDmg);
		var clone2 = Instantiate(damageNumber,other.transform.position, Quaternion.Euler(Vector3.zero));
		floatingNumbers.damageNumber = weapon.playerIntDmg;
		Instantiate(deathParticle, other.transform.position, other.transform.rotation);
		floatingNumbers._color = _color;
		floatingNumbers._text = "-";
		Destroy(gameObject);
	}
}