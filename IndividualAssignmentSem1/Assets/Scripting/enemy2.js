﻿#pragma strict
import UnityEngine.UI;

var enemyHealthMax:float;
var enemyHealth:float;
var healthbar:Image;

var theplayerAtt:playerAtt;
var coins:GameObject;
var target:Transform;
var speed:float=3.0;
var walk:boolean;
 var left:boolean;
 var right:boolean;
var attack:boolean=true;
var xDis:float;
var expToGive:int;
var j:int;
var radius:float;
var randomAtt:float;
private var anim :Animator;
var projectile:GameObject;
var spawn:GameObject;
function Start () 
{
	anim = GetComponentInChildren(Animator);
	theplayerAtt = FindObjectOfType(playerAtt);
	radius=1;
	spawn = GameObject.FindGameObjectWithTag("shoot");
		enemyHealth=enemyHealthMax;

}
function Awake()
{
	target = GameObject.FindGameObjectWithTag("Player").transform;
}
function Update () 
{
	healthbar.fillAmount = enemyHealth/enemyHealthMax;
	randomAtt-=Time.deltaTime;
	if(enemyHealth<=0)
	{
		Destroy(gameObject);
		theplayerAtt.AddExp(expToGive);
		for(j=0;j<=Random.Range(0,3);j++)
		{
			
			Instantiate(coins, Random.insideUnitSphere * radius+ transform.position, transform.rotation);
		}
		EnemyManager.eneNum-=1;
	}
	
	if(target!=null)
	{
		xDis=transform.position.x-target.position.x;
		if(xDis>8)
		{
			walk = true;
			anim.SetBool("walk",true);
			anim.SetBool("attack",false);
			transform.Translate(new Vector3(-speed*Time.deltaTime,0,0));
			transform.localScale=new Vector3(-0.8,0.8,1);
		}
		
		if(xDis<8)
		{
			walk = false;
			right=true;
			left=false;
			anim.SetBool("walk",false);
			transform.Translate(new Vector3(0,0,0));
			transform.localScale=new Vector3(-0.8,0.8,1);
			anim.SetBool("attack",true);
			
				
			
		}
		if(xDis<-8)
		{
			walk = true;
			anim.SetBool("walk",true);
			anim.SetBool("attack",false);
			transform.Translate(new Vector3(speed*Time.deltaTime,0,0));
			transform.localScale=new Vector3(0.8,0.8,1);
		}
		if(xDis>-8&&xDis<0)
		{
			walk = false;
			right=false;
			left=true;
			anim.SetBool("walk",false);
			transform.Translate(new Vector3(0,0,0));
			transform.localScale=new Vector3(0.8,0.8,1);
			anim.SetBool("attack",true);
		}
		else{
			walk=false;
		}
	}
}

function EnemyHit(playerdmg:int)
{
	enemyHealth -= playerdmg;
}
function Shoot()
{
	if(left==true)
	{	
		 var SPAWN_DISTANCE: int  = 5;
		Instantiate(projectile, transform.position + SPAWN_DISTANCE * transform.forward, transform.rotation);
		Projectile.movement = 4;
	}
	else
	{
		var SPAWN_DISTANCE2: int  = -5;
		Instantiate(projectile, transform.position + SPAWN_DISTANCE2 * Vector3.back, transform.rotation);
		Projectile.movement = -4;
	}
}