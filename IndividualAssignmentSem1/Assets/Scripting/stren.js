﻿#pragma strict
import UnityEngine.UI;

var displaySTR:Text;
var displayINT:Text;
var displaySkillPoints:Text;
var displayHP:Text;
var displayMP:Text;
var displayVit:Text;
var displayWis:Text;
var displayDex:Text;
var displayAtt:Text;
var displayDef:Text;
var displayCrit:Text;
var displaySpd:Text;

function Start()
{
}
function Update()
{

	displaySTR.text = "" + weapon.playerStr;
	displayINT.text = "" + weapon.playerInt;
	displayVit.text = "" + weapon.playerVit;
	displayWis.text = "" + weapon.playerWis;
	displayDex.text = "" + weapon.playerDex;
	displayAtt.text = "" + weapon.playerDmg;
	displaySpd.text = "" + weapon.playerSpd;
	displayDef.text = "" + playerAtt.armor;
	displaySkillPoints.text = "" + playerAtt.skillpoint;
	displayHP.text = "" + playerAtt.maxplayerHealth;
	displayMP.text = "" + playerAtt.maxplayerMana;
}
function StrAdd()
{
	if(playerAtt.skillpoint>0)
	{
		weapon.playerStr+=1;
		playerAtt.armor+=0.04;
		playerAtt.skillpoint--;
	}
}
function IntAdd()
{
	if(playerAtt.skillpoint>0)
	{
		weapon.playerInt=weapon.playerInt+1;
		playerAtt.skillpoint--;
	}
}

function VitAdd()
{
	if(playerAtt.skillpoint>0)
	{
		weapon.playerVit+=1;
		playerAtt.maxplayerHealth=playerAtt.maxplayerHealth+5;
		playerAtt.skillpoint--;
	}
}
function WisAdd()
{
	if(playerAtt.skillpoint>0)
	{
		weapon.playerWis+=1;
		playerAtt.maxplayerMana=playerAtt.maxplayerMana+5;
		playerAtt.skillpoint--;
	}
}
function DexAdd()
{
	if(playerAtt.skillpoint>0)
	{
		weapon.playerDex+=1;
		weapon.playerSpd = weapon.playerSpd+0.2;
		playerAtt.skillpoint--;
	}
}
function ResetSkills()
{
	weapon.playerStr =1;
	weapon.playerInt = 1;
	weapon.playerWis = 1;
	weapon.playerVit = 1;
	weapon.playerDex = 1;
	weapon.playerSpd = 5;
	playerAtt.maxplayerHealth=100;
	playerAtt.maxplayerMana = 100;
	playerAtt.skillpoint = playerAtt.maxSkillpoint;
}
function ExitObj()
{
	Destroy(gameObject);
	StatMenu.instanceNum--;
}
