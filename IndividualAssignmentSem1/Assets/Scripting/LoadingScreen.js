﻿#pragma strict
import UnityEngine.UI;

private var loadScene:boolean = false;
	
static var scene:int;
	
var loadingText:Text;
	
	
function Update() {
		
	if (loadScene == false) 
	{
			
		loadScene = true;
			
		loadingText.text = "Now Loading...";
			
		StartCoroutine(LoadNewScene());
			
	}
		
	if (loadScene == true) 
	{
			
		loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));
			
	}
		
}
	
function LoadNewScene() 
{
		
	yield WaitForSeconds(3);
		
	var async:AsyncOperation = Application.LoadLevelAsync(scene);
		
	while (!async.isDone) 
	{
		yield ;
	}
		
}