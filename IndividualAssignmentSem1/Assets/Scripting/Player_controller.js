﻿#pragma strict
import UnityEngine.UI;

var horizontal :float;
var sound:AudioClip[];

private var attack:boolean;
private var attack2:boolean;
private var anim :Animator;

private var myRigid :Rigidbody2D;

var obj:GameObject;
var moveSpeed :float;
var jumpSpeed:float;

private var grounded:boolean;
static var health:int=10;
static var right:boolean;
static var once_call:boolean;
var animStoper:float;

var skillTime:float;
private var skillbar:GameObject;
var skillTime2:float;
private var skillbar2:GameObject;

var whatIsGround:LayerMask;
var groundCheck:Transform;
var groundDistance:float;

var script : Player_controller; 
function Start () 
{
	myRigid = GetComponent(Rigidbody2D);
	anim = GetComponentInChildren(Animator);
	transform.position = GameObject.FindWithTag("SpawnPoint").transform.position;
}
function Awake()
{
	if (!once_call) {
DontDestroyOnLoad (this);
once_call = true;
} else {
Destroy (gameObject);
}
}
function Update()
{	
	if (GameObject.FindWithTag("skillbar") != null)
    {
    	skillbar = GameObject.FindGameObjectWithTag("skillbar");
        skillbar.GetComponent(Image).fillAmount = skillTime/10;
    }
	if (GameObject.FindWithTag("skillbar2") != null)
    {
    	skillbar2 = GameObject.FindGameObjectWithTag("skillbar2");
        skillbar2.GetComponent(Image).fillAmount = skillTime2/10;
    }
	skillTime -= Time.deltaTime;
	skillTime2 -= Time.deltaTime;
	animStoper -= Time.deltaTime;
	HandleInput();
	Spells();
}
function FixedUpdate () {

		var moveDir:Vector2 = new Vector2(Input.GetAxisRaw("Horizontal") * weapon.playerSpd, myRigid.velocity.y);

			myRigid.velocity = moveDir;
		
		if(Input.GetAxisRaw("Horizontal")==1)
		{
			weapon.playerSpd=5;
			transform.localScale = new Vector3(1,1,1);
			obj.transform.localScale = new Vector3(1,1,1);
			right = true;
			
		} 
		if(Input.GetAxisRaw("Horizontal")==-1){
			transform.localScale = new Vector3(-1,1,1);
			obj.transform.localScale = new Vector3(-1,-1,-1);
			right=false;
		}
		if(Input.GetKeyDown(KeyCode.Space))
        {
        	if(grounded)
        	{
           	 	myRigid.velocity = new Vector2(myRigid.velocity.x, jumpSpeed);
           	 	GetComponent.<AudioSource>().PlayOneShot(sound[3]);
            }
        }
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundDistance, whatIsGround);
        anim.SetBool("jump", !grounded);
		var clamp:Vector3 = transform.position;
		clamp.x = Mathf.Clamp(transform.position.x, -49.2, 49.2);
		transform.position = clamp;
		if(animStoper>0)
		{
			myRigid.velocity = Vector2.zero;
		}
		anim.SetFloat("Speed", Mathf.Abs( myRigid.velocity.x));
		ResetValues();
	
}
function HandleInput()
{
	if ( Input.GetKeyDown (KeyCode.K ) )
	{
 		anim.SetTrigger("attack");
 		GetComponent.<AudioSource>().PlayOneShot(sound[2]);
   	}
}
function ResetValues()
{
	attack = false;
	attack2 = false;
}
 function OnLevelWasLoaded()
 {
     transform.position = GameObject.FindWithTag("SpawnPoint").transform.position;
 }
 function Spells()
 {
 	if(Input.GetKeyDown(KeyCode.Alpha1))
 	{
 		if(playerAtt.curplayerMana>0&&skillTime<0)
 		{
 			gameObject.GetComponent(SpellManager).CastSpell(0);	
 			animStoper =2;
 			playerAtt.curplayerMana-=10;
 			playerAtt.curplayerHealth+=weapon.playerHealth;
 			skillTime = 10;
 			GetComponent.<AudioSource>().PlayOneShot(sound[0]);
 		}
 	}
 	if(Input.GetKeyDown(KeyCode.Alpha2))
 	{
 		if(playerAtt.curplayerMana>0&&skillTime2<0)
 		{
 			anim.SetTrigger("attack2");
 			animStoper =2.5;
 			playerAtt.curplayerMana-=10;
 			skillTime2 = 10;
 		}
 	}
 }
 function OnTriggerEnter2D(col: Collider2D) 
{
	Debug.Log ("Collision");
	if (col.gameObject.tag == "coins") 
    {
    	GetComponent.<AudioSource>().PlayOneShot(sound[1]);
        money.money+=5;
		Destroy(col.gameObject);
    }
}