﻿#pragma strict
var club:SpriteRenderer;
var spriteRenderer:SpriteRenderer;
var spriteSwords:Sprite[];
var weaponCost:float[];
static var costNum:int=0;
static var spriteNum:int=0;
var displayWeaponCost1:Text;
var displayWeaponCost2:Text;
var displayWeaponCost3:Text;
var displayWeaponCost4:Text;
var displayWeaponCost5:Text;
var displayWeaponCost6:Text;
var displayWeaponCost7:Text;
var displayWeaponCost8:Text;
var displayMoney:Text;
static var weaponNum:int;
var sound:AudioClip;
function Start () 
{
	club = GameObject.FindGameObjectWithTag("Weapon").GetComponent(SpriteRenderer);
}

function Update () 
{
	displayWeaponCost1.text = "" + weaponCost[0];
	displayWeaponCost2.text = "" + weaponCost[1];
	displayWeaponCost3.text = "" + weaponCost[2];
	displayWeaponCost4.text = "" + weaponCost[3];
	displayWeaponCost5.text = "" + weaponCost[4];
	displayWeaponCost6.text = "" + weaponCost[5];
	displayWeaponCost7.text = "" + weaponCost[6];
	displayWeaponCost8.text = "" + weaponCost[7];
	
	
}

function ExitObj()
{
	Destroy(gameObject);
	StatMenu.instanceNum--;
}
function BuyWeapon1()
{
	if(money.money>=weaponCost[0])
	{
		money.money=money.money-weaponCost[0];
		club.sprite = spriteSwords[0];
		weapon.curDmg=2;
		weaponNum=1;
		GetComponent.<AudioSource>().PlayOneShot(sound);
	}
}
function BuyWeapon2()
{
	if(money.money>=weaponCost[1])
	{
		money.money=money.money-weaponCost[1];
		club.sprite = spriteSwords[1];
		weapon.curDmg=3;
		weaponNum=2;
		GetComponent.<AudioSource>().PlayOneShot(sound);
	}
}
function BuyWeapon3()
{
	if(money.money>=weaponCost[2])
	{
		money.money=money.money-weaponCost[2];
		club.sprite = spriteSwords[2];
		weapon.curDmg=4;
		weaponNum=3;
		GetComponent.<AudioSource>().PlayOneShot(sound);
	}
}
function BuyWeapon4()
{
	if(money.money>=weaponCost[3])
	{
		money.money=money.money-weaponCost[3];
		club.sprite = spriteSwords[3];
		weapon.curDmg=5;
		weaponNum=4;
		GetComponent.<AudioSource>().PlayOneShot(sound);
	}
}
function BuyWeapon5()
{
	if(money.money>=weaponCost[4])
	{
		money.money=money.money-weaponCost[4];
		club.sprite = spriteSwords[4];
		weapon.curDmg=6;
		weaponNum=5;
		GetComponent.<AudioSource>().PlayOneShot(sound);
	}
}
function BuyWeapon6()
{
	if(money.money>=weaponCost[5])
	{
		money.money=money.money-weaponCost[5];
		club.sprite = spriteSwords[5];
		weapon.curDmg=7;
		weaponNum=6;
		GetComponent.<AudioSource>().PlayOneShot(sound);
	}
}
function BuyWeapon7()
{
	if(money.money>=weaponCost[6])
	{
		money.money=money.money-weaponCost[6];
		club.sprite = spriteSwords[6];
		weapon.curDmg=8;
		weaponNum=7;
		GetComponent.<AudioSource>().PlayOneShot(sound);
	}
}
function BuyWeapon8()
{
	if(money.money>=weaponCost[7])
	{
		money.money=money.money-weaponCost[7];
		club.sprite = spriteSwords[7];
		weapon.curDmg=9;
		weaponNum=8;
		GetComponent.<AudioSource>().PlayOneShot(sound);
	}
}