﻿#pragma strict

var sprite1:Sprite;
var sprite2:Sprite;
var spriteRenderer:SpriteRenderer;
function Start () 
{
    spriteRenderer = GetComponent(SpriteRenderer); 
    if (spriteRenderer.sprite == null)
    {// if the sprite on spriteRenderer is null then
        spriteRenderer.sprite = sprite1; 
    }
}

function Update () {
    if (Input.GetKeyDown (KeyCode.Space)) // If the space bar is pushed down
    {
        ChangeTheDamnSprite (); // call method to change sprite
    }
}
function ChangeTheDamnSprite ()
{
    if (spriteRenderer.sprite == sprite1) // if the spriteRenderer sprite = sprite1 then change to sprite2
    {
        spriteRenderer.sprite = sprite2;
    }
    else
    {
        spriteRenderer.sprite = sprite1; // otherwise change it back to sprite1
    }
}