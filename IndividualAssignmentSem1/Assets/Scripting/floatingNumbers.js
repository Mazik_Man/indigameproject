﻿#pragma strict
import UnityEngine.UI;

var floatSpeed:float;
static var damageNumber:float;
static var displayNumber:Text;
static var _color:Color;
static var _text:String;
function Start () 
{
	displayNumber = GetComponentInChildren(Text);
	displayNumber.color = _color;
	displayNumber.text = _text + damageNumber;
}

function FixedUpdate () 
{
	transform.position = new Vector3(transform.position.x, transform.position.y + (floatSpeed * Time.deltaTime), transform.position.z); 
}