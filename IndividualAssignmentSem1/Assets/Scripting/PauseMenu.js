﻿#pragma strict
var done:boolean=false;
var pauseMenu: GameObject;
function Start () {

}

function Update () 
{
	PauseSystem();
}
function PauseSystem()
{
	if(Input.GetKeyDown(KeyCode.Escape))
	{
		if(!done)
		{
			Pause();
		}
		else if(done)
		{
			UnPause();
		}
	}
}
function Pause()
{
	Time.timeScale =0;
	done=true;
	pauseMenu.SetActive(true);
}
function UnPause()
{
	Time.timeScale=1;
	done=false;
	pauseMenu.SetActive(false);
}